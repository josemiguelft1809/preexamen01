/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Jose Figueroa
 */
public class ProductosPerecederos extends Productos{
    
    private String fechaCaducidad;
    private float temperatura;
    
    public ProductosPerecederos() {
        this.fechaCaducidad = "";
        this.temperatura = 0.0f;
    }

    public ProductosPerecederos(String fechaCaducidad, float temperatura, int idProducto, String nombreProducto, int unidadProducto, float precioUnitario) {
        super(idProducto, nombreProducto, unidadProducto, precioUnitario);
        this.fechaCaducidad = fechaCaducidad;
        this.temperatura = temperatura;
    }

    public String getFechaCaducidad() {
        return fechaCaducidad;
    }

    public void setFechaCaducidad(String fechaCaducidad) {
        this.fechaCaducidad = fechaCaducidad;
    }

    public float getTemperatura() {
        return temperatura;
    }

    public void setTemperatura(float temperatura) {
        this.temperatura = temperatura;
    }
    
    
    
    @Override
    public float calcularPrecio() {
        float precio = 0.0f;
        if(this.unidadProducto ==0){
            precio = (float) (this.precioUnitario * 1.03);
        }
        if(this.unidadProducto ==1){
            precio = (float) (this.precioUnitario * 1.05);
        }
        if(this.unidadProducto ==2){
            precio = (float) (this.precioUnitario * 1.04);
        }
        
        return precio * 1.5f;
    }
    
}