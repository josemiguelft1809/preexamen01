/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Jose Figueroa
 */
public abstract class NotaVenta {
    protected int numNota;
    protected String fecha;
    protected String concepto;
    protected float cantidad;
    protected int tipoPago;
    protected Productos productosPerecederos;

    public NotaVenta() {
        this.numNota = 0;
        this.fecha = "";
        this.concepto = "";
        this.cantidad = 0.0f;
        this.tipoPago = 0;
        this.productosPerecederos = new ProductosPerecederos();
    }

    public NotaVenta(int numNota, String fecha, String concepto, float cantidad, int tipoPago, ProductosPerecederos productosPerecederos) {
        this.numNota = numNota;
        this.fecha = fecha;
        this.concepto = concepto;
        this.cantidad = cantidad;
        this.tipoPago = tipoPago;
        this.productosPerecederos = productosPerecederos;
    }

    public int getNumNota() {
        return numNota;
    }

    public void setNumNota(int numNota) {
        this.numNota = numNota;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getConcepto() {
        return concepto;
    }

    public void setConcepto(String concepto) {
        this.concepto = concepto;
    }

    public float getCantidad() {
        return cantidad;
    }

    public void setCantidad(float cantidad) {
        this.cantidad = cantidad;
    }

    public int getTipoPago() {
        return tipoPago;
    }

    public void setTipoPago(int tipoPago) {
        this.tipoPago = tipoPago;
    }

    public Productos getProductosPerecederos() {
        return productosPerecederos;
    }

    public void setProductosPerecederos(Productos productosPerecederos) {
        this.productosPerecederos = productosPerecederos;
    }
    
    public abstract float calcularPago();
    
    
}
