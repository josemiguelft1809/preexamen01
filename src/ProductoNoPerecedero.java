/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Jose Figueroa
 */
public class ProductoNoPerecedero extends Productos{
    
    private String loteFabricacion;

    public ProductoNoPerecedero() {
        this.loteFabricacion = "";
    }

    public ProductoNoPerecedero(String loteFabricacion) {
        this.loteFabricacion = loteFabricacion;
    }

    public ProductoNoPerecedero(String loteFabricacion, int idProducto, String nombreProducto, int unidadProducto, float precioUnitario) {
        super(idProducto, nombreProducto, unidadProducto, precioUnitario);
        this.loteFabricacion = loteFabricacion;
    }

    public String getLoteFabricacion() {
        return loteFabricacion;
    }

    public void setLoteFabricacion(String loteFabricacion) {
        this.loteFabricacion = loteFabricacion;
    }
    
    
    @Override
    public float calcularPrecio() {
        return (float) (this.precioUnitario * 1.5);
    }
}
