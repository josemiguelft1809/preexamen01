/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Jose Figueroa
 */
public class Factura extends NotaVenta implements Iva{
    private String nombre;
    private String rfc;
    private String domicilio;

    public Factura() {
        this.nombre = "";
        this.rfc = "";
        this.domicilio = "";
    }

    public Factura(String nombre, String rfc, String domicilio, int numNota, String fecha, String concepto, float cantidad, int tipoPago, ProductosPerecederos productosPerecederos) {
        super(numNota, fecha, concepto, cantidad, tipoPago, productosPerecederos);
        this.nombre = nombre;
        this.rfc = rfc;
        this.domicilio = domicilio;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    @Override
    public float calcularPago() {
        return this.productosPerecederos.calcularPrecio() * cantidad;
    }

    @Override
    public float calcularIva() {
        return (float) (this.calcularPago() * 0.16);
    }
}
